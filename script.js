const baseUrl = "https://lanbide-node.herokuapp.com/admins";

$(document).ready(function(){
  $("#btnGetAll").click(onClick_getAll);
});

function onClick_getAll(){
  console.log("getAll");
  $("#btnGetAll").addClass("clicked");
  lanzarAjax(null, baseUrl, "GET");
}

function lanzarAjax(data, baseUrl, type) {
      $.ajax({
          data: data,
          type: type,
          dataType: "json",
          url: baseUrl 
      })
      .done(function( data, textStatus, jqXHR ) {
         // console.log("La solicitud se ha completado correctamente.");
         // hideLoading(	);
         if( type === "GET") {
         	console.log("Got all data succefully");
         	printResults(data); 
         } 
         else if (type==="POST") {
         	console.log("Sent a new data succefully");
         }
         else if (type==="PUT"){
         	console.log("Updated succefully");
            onClick_getAll()
         }
         else {
         	console.log("Deleted succefully");
         }
          
       })
      .fail(function( jqXHR, textStatus, errorThrown ) {
          console.log( "La solicitud a fallado: " +  textStatus);
      });
}

function printResults(data) {
    $("#tbody").html("");
	//console.table(data.admins);
  for(var i = 0; i < data.admins.length; i++) {
//    printNumber(data.admins[i], i);
    printContainer(data.admins[i]);
    printAvatar(data.admins[i]);
    printName(data.admins[i]);
    printUsername(data.admins[i]);
    printEmail(data.admins[i]);
    printButtonsTd(data.admins[i]);
    printActive(data.admins[i]);
    printEdit(data.admins[i]);
    printDelete(data.admins[i]);
  }
}

function printContainer(data) {
    jQuery('<thead/>', 
    {
      id: "thead_" + data._id
    }).appendTo("#tabl"); 
    jQuery('<th/>', {id:"th_"+ data._id, text:"Foto"}).appendTo("#thead_"+ data._id);
    jQuery('<th/>', {id:"th_"+ data._id, text:"Username"}).appendTo("#thead_"+ data._id);
    jQuery('<th/>', {id:"th_"+ data._id, text:"Name"}).appendTo("#thead_"+ data._id);
    jQuery('<th/>', {id:"th_"+ data._id, text:"Email"}).appendTo("#thead_"+ data._id);
    jQuery('<th/>', {id:"th_"+ data._id, text:"Manegar perfil"}).appendTo("#thead_"+ data._id);

    jQuery('<tr/>', 
    {
      id: "box_" + data._id,
      class: ""
    }).appendTo("#tbody"); 
}
/*
function printNumber(i) {
    jQuery('<td/>', {
        id:"number_"+ data._id, 
        text: i
    })
        .appendTo("#box_"+ data._id);
    }
*/


function printAvatar(data) {
	var ruta;
	if (data.avatar=== null || data.avatar==="undefined"){
		ruta= "./avatar.png";
	}
	else{
		ruta=data.avatar;
	}
    jQuery('<td/>', {id:"td_"+ data._id}).appendTo("#box_"+ data._id);
    jQuery('<img/>', 
    {
      src: ruta,
      class: "avatar"
    }).appendTo("#td_"+ data._id); 
    
}

function printName(data) {
/*	var nombre;
if (data.name=== null || data.name==="undefined"){
		nombre= "Sin nombre";
*/
  jQuery('<td/>', 
    {
      text: data.name
    }).appendTo("#box_"+ data._id); 
}

function printUsername(data) {
  jQuery('<td/>', 
    {
      text: data.username
    }).appendTo("#box_"+ data._id); 
}

function printEmail(data) {
  jQuery('<td/>', 
    {
      text: data.email
    }).appendTo("#box_"+ data._id); 
}

function printButtonsTd(data){
    jQuery('<td/>', {id:"but_"+ data._id}).appendTo("#box_"+ data._id);
}
function printActive(data) {
    var activetext;
    var class_;
	if (!data.active) { // true si usuario desactivado
	  	activetext="Activar";
        class_="btn btn-warning disabled";
        active_=true;
	} else { // si usuario activado
  		activetext="Desactivar";
        class_="btn btn-warning";
        active_=false;
  	}
    jQuery("<button/>", {
  			id:"btnDes_"+data._id,
  			text:activetext,
            class: "btn btn-warning"
  		}).appendTo("#but_"+data._id);
  		$("#btnDes_"+data._id).click(function(){
	  		console.log("Botón desactivar " + data._id);
            //preparamos peticion
            data.active=active_;
            var url= baseUrl + "/" + data._id
            lanzarAjax(data, url, "PUT");
        //    $("#btnEdit_"+data._id).addClass("disabled");
          //  $("#btnDel2_"+data._id).addClass("disabled");
	  	})
}
function printEdit(data){
    
	if (data.active) { // si usuario esta deactivado = true
		jQuery('<button/>', 
    {
      id:"btnEdit_"+data._id,
      text:"Edit",
      class: "btn btn-success"
    }).appendTo("#but_"+ data._id);
	//
	$("#btnEdit_"+data._id).click(
  	function(){
        console.log("Botón edit for desactivated "+data._id);
  				})
}
	else
{
		jQuery('<button/>', 
    {
        id:"btnEdit_"+data._id,
        text:"Edit",
        class: "btn btn-success disabled"
      }).appendTo("#but_"+data._id)
        $("#btnEdit_"+data._id).click(
  	 function(){
  	     console.log("Botón edit for activated "+data._id);
  				})
	}
}

function printDelete(data){
    
	if (data.active) {
		jQuery('<button/>', 
    {
      id:"btnDel2_"+data._id,
      text:"Delete",
      class: "btn btn-danger"
    }).appendTo("#but_"+ data._id);
	//
	$("#btnDel2_"+data._id).click(
  	function(){
        console.log("Botón Delete for desactivated "+data._id);
  				})
}
	else
{
		jQuery('<button/>', 
    {
        id:"btnDel2_"+data._id,
        text:"Delete",
        class: "btn btn-danger disabled"
      }).appendTo("#but_"+data._id)
        $("#btnDel2_"+data._id).click(
  	 function(){
  	     console.log("Botón Delete for activated "+data._id);
  				})
	}
}

function delete_data(data){
        $.ajax({
        data: data,
        type: "DELETE",
        dataType: "json",
        url: "https://lanbide-node.herokuapp.com/admins" 
    })
        .done(function(data, textStatus, jqXHR ) {
            covsole.log(delete {"_id":data}); // aqui me da falto que 404
            console.log("sent success");
    })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            console.log($.ajax);
            console.log( "sent failed: " +  textStatus);
    });

}

/*
function delete_data(data){
        $.ajax({
        data: data,
        type: "PUT",
        dataType: "json",
        url: "https://lanbide-node.herokuapp.com/admins" 
    })
        .done(function(data, textStatus, jqXHR ) {
            covsole.log(PUT {"_id":data}); // aqui me da falto que 404
            console.log("sent success");
    })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            console.log($.ajax);
            console.log( "sent failed: " +  textStatus);
    });

}
*/
